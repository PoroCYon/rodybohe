
#ifndef SND_H_
#define SND_H_

#include "def.h"
#include "type.h"
#include "sys.h"

#ifndef ENABLE_AUDIO
#define snd_init() do{}while(0)
#define snd_kill() do{}while(0)
#define snd_gen(buf,t,i) i
#else
forcedinline void snd_init(void) {
    SYS_fcntl(STDOUT_FILENO, F_SETFL,
        (size_t)SYS_fcntl(STDOUT_FILENO, F_GETFL, 0)|O_NONBLOCK);
}
forcedinline void snd_kill(void) {
    // will bork if called without snd_init being called
    SYS_fcntl(STDOUT_FILENO, F_SETFL,
        (size_t)SYS_fcntl(STDOUT_FILENO, F_GETFL, 0)^O_NONBLOCK);
}

#endif

#endif

