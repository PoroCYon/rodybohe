
#ifndef TEX_H_
#define TEX_H_

#include <stddef.h>
#include <stdint.h>

#include "def.h"

#define TEXGENFNS (9)
#define TEXGENARR (8)

extern uint8_t randdata[0x10];

// stuff will break if fnarr[i] >= TEXGENFNS for any i
forcedinline size_t tex_gen1_c(size_t seed, const uint8_t fnarr[TEXGENARR]) {
    register size_t retv
#if defined(__x86_64__)
        asm("rax")
#else
        asm("eax")
#endif
        ;

    asm volatile("call tex_gen1\n"
        :"=a" (retv)
        :"a" (seed), "S" (fnarr)
#if defined(__x86_64__)
        :"rbx", "rcx", "rdx" //, "rsi"
#else
        :"ebx", "ecx", "edx" //, "esi"
#endif
        );

    return retv;
}

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wunused-function"
/*forcedinline*/ static void tex_gen_ind(size_t size,
        size_t* dest, const uint8_t fnarr[TEXGENARR]) {
    size_t i = size;
    do { *dest = tex_gen1_c(i, fnarr); ++dest; --i; } while (i);
}
/*forcedinline*/ static void tex_gen_feedback(size_t seed, size_t size,
        size_t* dest, const uint8_t fnarr[TEXGENARR]) {
    size_t i = size;
    do { *dest = seed = tex_gen1_c(seed, fnarr); ++dest; --i; } while (i);
}
#pragma GCC diagnostic pop

#endif

