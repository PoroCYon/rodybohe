
#ifndef ZAN_TYPE_H_
#define ZAN_TYPE_H_

#include <stddef.h>
#include <stdint.h>
#include <stdbool.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <signal.h>
#include <time.h>
#include <fcntl.h>
#include <linux/fb.h>

#ifndef __x86_64__
struct mmap_arg_struct {
    void* addr;
    size_t len;
    size_t prot;
    size_t flags;
    ssize_t fd;
    off_t offset;
};
#endif

#endif

