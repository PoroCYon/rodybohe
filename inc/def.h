
#ifndef DEF_H_
#define DEF_H_

#define forcedinline __attribute__((__always_inline__)) static inline

#define PRINT_DEBUG_IN_RELEASE_MODE

//#define DISABLE_FBDEV

// +13B??
//#define INFINITE_LOOP

#define ENABLE_AUDIO

//#define ENABLE_FONT

// disable all I/O though stdin/stdout
//#define NOSTDIO
#ifdef NOSTDIO
#ifdef ENABLE_FONT
#undef ENABLE_FONT
#endif
#ifdef ENABLE_AUDIO
#undef ENABLE_AUDIO
#endif
#endif

// directly expand the font data in memory after reading,
// could save some bytes when blitting a lot in different
// places
//#define EXPAND_FONT_IN_MEM

// enable this when in need of extra bytes, while removing some minor
// stuff.
// NOTE: gains 106 bytes last time I checked
//#define DESPERATE      /* 1076 -> 1174 */
//#define VERY_DESPERATE /*  998 -> 1076 */

// if this isn't defined, it is queried at runtime
#define FBWIDTH  (/*1920*/ 1366)
#define FBHEIGHT (/*1080*/  768)
// define this if you want to take the offset into account
//#define FB_OFFSETS

// audio buffer size
#define FBBUFSZ  (0x2000)
#define AUBUFSZ  (0x0400)
#define AUONESEC (0x8000)
#define AURINGSZ (AUONESEC<<1) /* buffer size of a pipe */

// manually align the stack on x86_64, disabled when DESPERATE is enabled
// currently not needed
//#define ALIGN_STACK

// assume var.bits_per_pixel == 32, because we do everywhere else anyway
#if defined(FBWIDTH) && defined(FBHEIGHT)
#define ADDRESS_FB(x,y) (uint32_t*)(bmem + (y) * fix.line_length + ((x)<<2))
#elif defined(FB_OFFSETS)
#define ADDRESS_FB(x,y) (uint32_t*)(bmem + (var.yoffset + (y)) * fix.line_length + ((var.xoffset + (x))<<2))
#else
#define ADDRESS_FB(x,y) (uint32_t*)(bmem + (y) * fix.line_length + ((x)<<2))
#endif

// font width, height
#define FONTW      (       8)
#define FONTH      (       8)
#define FONTCHARS  (   0x100)
// file size (assuming 'raw' font file)
#define FONTFILESZ (FONTH<<8)

#define GETFONTBIT(glyph,x,y) ((glyph)[y] & (1 << ((uint8_t)7^(uint8_t)(x))))

// don't break on \n in render_str, \n is unused for now & shaves off ~45 bytes
#define NOFONTNL

#endif

