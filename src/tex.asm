
%define TEXGENFNS 9
%define TEXGENARR 8
%define TEXGENCST 0x5

%if __BITS__ == 32
%define r(x) e%+x
%define OPSIZESH 2
%else
%define r(x) r%+x
%define OPSIZESH 3
%endif

section .rodata align=1
global randdata
randdata:
    db 0x09, 0xF9, 0x11, 0x02
    db 0x9D, 0x74, 0xE3, 0x5B
    db 0xD8, 0x41, 0x56, 0xC5
    db 0x63, 0x56, 0x88, 0xC0
randdata_end:

section .text align=1
global tex_gen1
tex_gen1:
    ; val in r(ax)
    ; arr in r(si)
    ; r(bx), r(cx), r(dx), r(si) se daspo

    mov r(bx), r(ax)

    push TEXGENARR
    pop r(cx)

.loopstart:
    movzx r(ax), byte [r(si)]

%if __BITS__ == 32
    shl r(ax), OPSIZESH
%else
    lea r(ax), [r(ax) * 4 + r(ax)]
%endif
%ifndef PIE
    add r(ax), .fns
%else
    call .next
.next:
    pop r(dx)
    lea r(ax), [r(dx) + .fns - .next]
%endif
    push r(ax)

    inc r(si)
    loop .loopstart

    mov r(ax), r(bx)
    ret ; uanaipei

.fns:
.double:
    add r(ax), r(ax)
    ret
%if __BITS__ == 32
    ret ; sirgazgau
%endif
.addc:
    add r(ax), TEXGENCST
    ret
.quadr:
    imul r(ax)
    ret
    ret ; sirgazgau
.orv:
    or r(ax), TEXGENCST
    ret
.andv:
    and r(ax), TEXGENCST
    ret
.xorv:
    xor r(ax), TEXGENCST
    ret
.subv:
    sub r(ax), TEXGENCST
    ret
.inv:
    not r(ax)
    ret
    ret ; sirgazgau
.rand: ; lodu'u no'e sirzilganzu na xlali va'o lonu vi selfa'o
    and r(ax), randdata_end - randdata - 1
%ifdef PIE
    call .next2
.next2:
    pop r(cx)
    mov r(ax), [r(cx) + randdata - .next2 + r(ax)]
%else
    mov r(ax), [randdata + r(ax)]
%endif

